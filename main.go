package main

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/buger/jsonparser"
	"github.com/go-redis/redis"
)

const LIMIT = 2000

func main() {
	rds := conn()
	defer rds.Close()

	mux := http.NewServeMux()

	mux.HandleFunc("/apiv1/api/daftar", apiRegistrationHandler(rds))
	mux.HandleFunc("/apiv1/api/tarik", apiCashoutHandler(rds))
	mux.HandleFunc("/apiv1/sms/inbound", smsHandler(rds))
	mux.HandleFunc("/apiv1/api/bayar", buyTransaction(rds))
	mux.HandleFunc("/apiv1/api/report", report(rds))
	http.ListenAndServe(":8080", mux)
}

func conn() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     "35.198.246.221:9967",
		Password: "t3guhCakep", // no password set
		DB:       1,  // use default DB
	})
	return client
}
func hash(data string) string {
	h256 := sha256.New()
	io.WriteString(h256, data)
	return fmt.Sprintf("%x", h256.Sum(nil))
}

func sendSms(otp string, phone string) (err error) {

	url := "http://sms-api.jatismobile.com/index.ashx?channel=2&uploadby=test&batachname=test&division=OPERASI%20DAN%20LAYANAN%20TEKNOLOGI&sender=KANTOR%20POS&message=SMSgatewaytest&msisdn=6285646770852&password=POSINDO879&userid=POSINDO"
	req, _ := http.NewRequest("GET", url, nil)
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	message := string(body)
	firstSplit := strings.Split(message, "&")
	secSplit := strings.Split(firstSplit[0], "=")
	status := secSplit[1]

	if status != string(1) {
		err = errors.New("Error Send SMS")
	}

	return err
}

func smsHandler(redist *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		body, _ := ioutil.ReadAll(r.Body)
		code, _ := jsonparser.ParseString(body)
		bodyRawString := strings.Split(code, "&")[10]
		from := strings.Split(strings.Split(code, "&")[17], "=%2B")[1]
		bodyString := strings.Trim(strings.ToLower(strings.Split(bodyRawString, "=")[1]), "")
		if strings.HasPrefix(bodyString, "reg+") {
			registration(code,redist)
		} else if strings.HasPrefix(bodyString, "tarik+") {
			cashout(code,redist)
		} else {
			smsResponseHandler(from, "Format pesan yang kamu kirim salah")
		}
	}
}

func apiRegistrationHandler(redist *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		message:=`{"message":"Sukses Mendaftar Merchant"}`
		code := http.StatusOK
		body, _ := ioutil.ReadAll(r.Body)
		phone,_ := jsonparser.GetString(body, "phone")
		merchantCode,_:=jsonparser.GetString(body, "merchantcode")
		accountNumber,_:=jsonparser.GetString(body, "accountnumber")
		msg := validateApiRegistrationHandler(phone,merchantCode,accountNumber)
		if(len(msg)<=0){
			status := saveRegistration(merchantCode, phone, accountNumber,redist)
			if(status != 200){
				message = `{"message":"Merchant Id sudah terdaftar"}`
			}
		}else{
			message = fmt.Sprintf(`{"message":"parameter :%s kosong"}`,msg)
			code = http.StatusBadRequest
		}
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func validateApiRegistrationHandler(phone string,merchantCode string,accountNumber string)(message string){
	message = ""
	if(len(phone) <= 0){
		message = "phone"
	}else if(len(merchantCode) <= 0) {
		message = "merchantcode"
	}else if(len(accountNumber) <= 0) {
		message = "accountnumber"
	}
	return message
}

func registration(code string,redist *redis.Client) {
	bodyRawString := strings.Split(code, "&")[10]
	from := strings.Split(strings.Split(code, "&")[17], "=%2B")[1]
	bodyString := strings.Trim(strings.ToLower(strings.Split(bodyRawString, "=")[1]), "")
	bodyPrefix := strings.HasPrefix(bodyString, "reg+")
	if bodyPrefix {
		merchantCodeString := strings.Split(bodyString, "+")[1]
		accountNumber := strings.Split(bodyString, "+")[2]
		saveRegistration(merchantCodeString, from, accountNumber,redist)
	} else {
		smsResponseHandler(from, "Format pesan yang kamu kirim salah")
	}
}

func saveRegistration(merchantCode string, phone string, accountNumber string,redist *redis.Client)(code int) {
	code = 200
	t := time.Now()
	dates := t.Format("20060102")
	keys := dates + "|" + phone + "|" + merchantCode + "|in"
	_, err := redist.Get(keys).Result()
	if err == redis.Nil {
		pincode := genPincode()
		redist.Set(strconv.FormatUint(pincode, 10), merchantCode, 0)
		redist.Set(merchantCode, phone+"|"+accountNumber, 0)
		redist.LPush(keys, "init")
		smsResponseHandler(phone, "Selamat anda berhasil mendaftar menjadi merchant Digiroin. Pincode anda adalah "+strconv.FormatUint(pincode, 10)+". Simpan dan rahasiakan Pincode untuk fungsi administrasi kedepannya")
	} else {
		init := strings.Trim(strings.ToLower(redist.LRange(keys, 0, 0).Val()[0]), "")
		if len(init) != 0 {
			smsResponseHandler(phone, "Merchant Id sudah terdaftar")
			code = 405
		}
	}
	return code
}

func smsResponseHandler(phone string, message string) (err error) {
	messages := strings.Replace(message, " ", "%20", -1)
	url := "http://sms-api.jatismobile.com/index.ashx?channel=2&uploadby=test&batachname=test&division=OPERASI%20DAN%20LAYANAN%20TEKNOLOGI&sender=KANTOR%20POS&message=" + messages + "&msisdn=" + phone + "&password=POSINDO879&userid=POSINDO"
	req, _ := http.NewRequest("GET", url, nil)
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	resp := string(body)
	firstSplit := strings.Split(resp, "&")
	secSplit := strings.Split(firstSplit[0], "=")
	status := secSplit[1]

	if status != "1" {
		err = errors.New("Error Send SMS")
	}
	return err
}

func genPincode() (result uint64) {
	redis := conn()
	pincode := random()
	pincodeExist := redis.Get(strconv.FormatUint(pincode, 10)).Val()
	if pincodeExist != "" {
		genPincode()
	} else {
		result = pincode
	}
	return result
}

func buyTransaction(redist *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		result := ""
		code := 200
		body, _ := ioutil.ReadAll(r.Body)
		merchantCode, _ := jsonparser.GetString(body, "to")
		publicKey, _ := jsonparser.GetString(body, "from")
		amount, _ := jsonparser.GetInt(body, "amount")
		t := time.Now()
		dates := t.Format("20060102")
		phoneAccountNumber := redist.Get(merchantCode).Val()
		if phoneAccountNumber == "" {
			result = `{"message":"Merchant tidak terdaftar"}`
			code = 404
		} else {
			phone := strings.Split(phoneAccountNumber, "|")[0]
			keys := dates + "|" + phone + "|" + merchantCode + "|in"

			_, err := redist.Get(keys).Result()
			if err != redis.Nil {
				init := strings.Trim(strings.ToLower(redist.LRange(keys, 0, 0).Val()[0]), "")
				if strings.Compare(init, "init") == 0 {
					redist.LPop(keys)
				}
			}

			unixtime := int32(time.Now().Unix())

			jsonVal := `{"timestamp":` + strconv.FormatInt(int64(unixtime), 10) + `,"from":"` + publicKey + `","amount":` + strconv.FormatInt(amount, 10) + `}`

			stringToSHA256 := hash(jsonVal)
			redist.LPush(keys, jsonVal+"|"+stringToSHA256)
			result = `{"trx" : "` + stringToSHA256 + `","code":"` + strconv.Itoa(code) + `","message":"Pembayaran pada merchant ` + merchantCode + ` sejumlah ` + strconv.FormatInt(amount, 10) + ` Sukses"}`
		}
		w.Write([]byte(result))
		w.WriteHeader(code)
	}
}

func random() uint64 {
	rand.Seed(time.Now().Unix())
	val := uint64(rand.Intn(9999-1000) + 1000)
	return val
}

func apiCashoutHandler(redist *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		message:=`{"message":"Sukses Melakukan Tarik Uang"}"`
		code := http.StatusOK
		body, _ := ioutil.ReadAll(r.Body)
		phone,_ := jsonparser.GetString(body, "phone")
		merchantCode,_:=jsonparser.GetString(body, "merchantcode")
		pincode,_:=jsonparser.GetString(body, "pincode")
		msg := validateApiCashoutHandler(phone,merchantCode,pincode)
		if(len(msg)<=0){
			status := cashoutHandler(phone,merchantCode, pincode,redist)
			if(status == 400){
				message = fmt.Sprintf(`{"message":"Saldo Kurang dari : %d"}"`,LIMIT)
			}else if(status == 500){
				message = `{"message":"Tarik Uang Gagal"}"`
			}else if(status == 404){
				message = `{"message":"Merchant tidak terdaftar"}"`
			}
			code = status
		}else{
			message = fmt.Sprintf(`{"message":"parameter :%s kosong"}"`,msg)
			code = http.StatusBadRequest
		}
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func validateApiCashoutHandler(phone string,merchantCode string,pincode string)(message string){
	message = ""
	if(len(phone) <= 0){
		message = "phone"
	}else if(len(merchantCode) <= 0) {
		message = "merchantcode"
	}else if(len(pincode) <= 0) {
		message = "pincode"
	}
	return message
}

func cashout(code string, redist *redis.Client) {
	bodyRawString := strings.Split(code, "&")[10]
	from := strings.Split(strings.Split(code, "&")[17], "=%2B")[1]
	bodyString := strings.Trim(strings.ToLower(strings.Split(bodyRawString, "=")[1]), "")
	bodyPrefix := strings.HasPrefix(bodyString, "tarik+")
	if bodyPrefix {
		merchantCode := strings.Split(bodyString, "+")[1]
		pincode := strings.Split(bodyString, "+")[2]
		cashoutHandler(from, merchantCode, pincode, redist)
	} else {
		smsResponseHandler(from, "Format pesan yang kamu kirim salah")
	}
}

func cashoutHandler(phone string, merchantCode string, pincode string,redist *redis.Client)(code int) {
	merchantCodeExist := redist.Get(pincode).Val()
	if merchantCode == "" {
		code = 404
		smsResponseHandler(phone, "Merchant tidak terdaftar")
	} else if merchantCodeExist != merchantCode {
		code = 404
		smsResponseHandler(phone, "Merchant tidak terdaftar atau pincode salah")
	} else {
		t := time.Now()
		dates := t.Format("20060102")
		keys := dates + "|" + phone + "|" + merchantCode + "|in"
		_, err := redist.LRange(keys, 0, -1).Result()
		if err != redis.Nil {
			lists := redist.LRange(keys, 0, -1).Val()
			sum := 0
			for _, v := range lists {
				amount, _ := jsonparser.GetInt([]byte(v), "amount")
				sum = sum + int(amount)
			}

			if sum <= LIMIT {
				code = 400
				smsResponseHandler(phone, fmt.Sprintf("Saldo Kurang dari :%d",LIMIT))
			} else {
				phoneAccountNumber := redist.Get(merchantCode).Val()
				accountNumberBankCode := strings.Split(phoneAccountNumber, "|")[1]
				bankCode := accountNumberBankCode[0:3]
				accountNumber := accountNumberBankCode[3:len(accountNumberBankCode)]
				url := "http://35.198.202.232:7068/digiroin/cashout/va/cimb"
				payload := strings.NewReader("")
				if strings.Compare(bankCode, "022") == 0 {
					payload = strings.NewReader("{\"AccountFrom\":\"860005525500\",\"AccountTo\":\"" + accountNumber + "\",\"AccountToName\":\"A\",\"Amount\":\"" + strconv.Itoa(sum) + "\",\"PhoneNumber\":\"081213631232\"}")
				} else {
					payload = strings.NewReader("{\"AccountFrom\":\"860005525500\",\"AccountTo\":\"" + accountNumber + "\",\"AccountToName\":\"A\",\"Amount\":\"" + strconv.Itoa(sum) + "\",\"PhoneNumber\":\"081213631232\",\"AccountToBankCode\":\"" + bankCode + "\",\"AccountToBankName\":\"BANK CIMB\"}")
				}
				req, _ := http.NewRequest("POST", url, payload)
				req.Header.Add("content-type", "application/json")
				res, _ := http.DefaultClient.Do(req)

				defer res.Body.Close()
				if res.StatusCode == 200 {
					keysout := dates + "|" + phone + "|" + merchantCode
					for _, v := range lists {
						redist.LPush(keysout+"|out", v)
					}
					redist.Del(keys)
					code = 200
					smsResponseHandler(phone, "Sukses Melakukan Tarik Uang Sebesar "+strconv.Itoa(sum))
				} else {
					code = 500
					smsResponseHandler(phone, "Tarik Uang Gagal")
				}
			}
		} else {
			code = 404
			smsResponseHandler(phone, "Merchant tidak terdaftar")
		}
	}
	return code
}

func report(redist *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		body, _ := ioutil.ReadAll(r.Body)
		param, _ := jsonparser.GetString(body, "param")
		code := http.StatusOK

		keys := param + "*"
		result := ""
		listKeys := redist.Keys(keys).Val()
		for _, v := range listKeys {
			listValue := redist.LRange(v, 0, -1).Val()
			merchantId := strings.Split(v, "|")[2]
			status := strings.Split(v, "|")[3]
			for _, val := range listValue {
				timestamp, _ := jsonparser.GetInt([]byte(val), "timestamp")
				from, _ := jsonparser.GetString([]byte(val), "from")
				amount, _ := jsonparser.GetInt([]byte(val), "amount")

				tm := time.Unix(timestamp, 0)
				times := tm.Format("Mon Jan _2 15:04:05 2006")
				if(amount!=0){
					if strings.Compare(result, "") != 0 {
						result = result + ","
					}
					result = result + `{"timestamp":"` + times + `","merchantId":"` + merchantId + `","from":"` + from + `","amount":"` + strconv.FormatInt(amount, 10) + `","status":"` + status + `"}`
				}
			}
		}
		if (strings.Compare(result, "") == 0) {
			result = `{"message":"no data found"}`
			code = http.StatusNotFound
		}
		response := `{"data":[` + result + `]}`
		w.WriteHeader(code)
		w.Write([]byte(response))
	}
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
